package model.effects.convolution;

/**
 * Represents the edge detection grades .
 */
public enum EdgeDetectionGrade {
    /**
     * grade of edge detection .
     */
    LOW, MEDIUM, HIGH
}
